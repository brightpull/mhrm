<footer class="hide-print text-center md:text-left">

    <div class="py-8 text-white" style="background-color: #323232">

        <div class="container md:flex justify-center items-center">

            <p class="font-bold md:mb-0 text-xl">Do you need help?</p>

            <p class="mb-0 md:ml-8">
                <a class="u-button" href="http://www.mhcc.org.au/emergency-contact" target="_blank" style="background: #3d9b34;">
                    <?php echo get_template_part( 'svg/phone' ); ?>
                    <span>Emergency Contact</span>
                </a>
            </p>

        </div>

    </div>

    <div class="py-12 text-white" style="background-color: #252525">

        <div class="container">

            <div class="md:flex">

                <ul class="md:mr-20">
                    <li><a href="http://www.mhcc.org.au/about-mhcc" target="_blank">About Us</a></li>
                    <li><a href="http://www.mhcc.org.au/our-work/" target="_blank">Our Work</a></li>
                    <li><a href="http://www.mhcc.org.au/learning-development/" target="_blank">Training</a></li>
                    <li><a href="http://www.mhcc.org.au/membership/" target="_blank">Membership</a></li>
                </ul>

                <ul class="md:mr-16">
                    <li><a href="http://www.mhcc.org.au/events/" target="_blank">MHCC Events</a></li>
                    <li><a href="http://www.mhcc.org.au/our-work/blog/" target="_blank">News</a></li>
                    <li><a href="http://www.mhcc.org.au/our-work/resources/" target="_blank">Resources</a></li>
                    <li><a href="http://www.mhcc.org.au/membership/our-members/" target="_blank">Members Directory</a></li>
                    <li><a href="http://www.mhcc.org.au/contact-us/" target="_blank">Contact Us</a></li>
                </ul>

                <p>
                    <span class="text-base font-semibold">Subscribe to our newsletter</span>
                    <a class="u-button--white mt-3" href="http://www.mhcc.org.au/join-newsletter/" target="_blank">
                        <?php echo get_template_part( 'svg/mail' ); ?>
                        <span>Join Newsletter</span>
                    </a>
                </p>

                <p class="md:ml-auto">
                    <a href="http://www.mhcc.org.au/" target="_blank">
                        <img class="inline-block mb-2" width="150" src="<?php echo get_template_directory_uri(); ?>/svg/mhcc-logo-reverse.svg" alt="mhcc logo">
                    </a><br>
                    <span class="text-xs opacity-50">RTO number: 91296<br><br></span>
                    <span class="opacity-50 text-base">Connect with us</span><br>
                    <span class="footer__socials">
                        <a href="http://www.youtube.com/user/MHCCtv" target="_blank">
                            <?php echo get_template_part( 'svg/youtube' ); ?>
                        </a>
                        <a href="https://www.facebook.com/mhcc.nsw" target="_blank">
                            <?php echo get_template_part( 'svg/facebook' ); ?>
                        </a>
                        <a href="https://twitter.com/MHCC_NSW" target="_blank">
                            <?php echo get_template_part( 'svg/twitter' ); ?>
                        </a>
                    </span>

                </p>

            </div>

            <p class="mb-0 md:mt-5">
                Website &copy; MHCC | <a class="text-orange" href="http://www.mhcc.org.au/privacy_policy/" target="_blank">Privacy Policy</a> | Website by <a class="text-orange" href="https://brightagency.com.au" target="_blank">Bright</a>
            </p>

        </div>

    </div>

</footer>

<?php wp_footer(); ?>

<!-- Google Analytics -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', ' UA-35616493-1']);
  _gaq.push(['_setDomainName', 'mhcc.org.au']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</body>

</html>
