// Media queries
var smallDown = window.matchMedia("(max-width: 767px)");
var mediumUp = window.matchMedia("(min-width: 768px)");

// Slide toggle chapters on mobile
$('.js-show-chapters').click(function(e) {
    $('.submenu-chapters').slideToggle();
});

// Chapter menu items
$('.submenu > li > a').click(function(e) {
    e.preventDefault();

    $('.submenu > li > a').removeClass('active');
	$(this).addClass('active');

    $('.submenu > li > a:not(.active)').siblings('.children').slideUp();
	$(this).siblings('.children').slideToggle();
});

// On page load
$(document).ready(function() {
    if (mediumUp.matches) {
        $('.submenu li.current_page_item').closest('.children').show();
        $('.home .submenu-pages ul.children').hide();
    }
    if (smallDown.matches) {
        $('.submenu li.current_page_item').closest('.children').hide();
    }
});

// On resize
$(window).resize(function() {
    if (mediumUp.matches) {
        $('.submenu li.current_page_item').closest('.children').show();
        $('.submenu-chapters').show();
    }
    if (smallDown.matches) {
        $('.submenu li.current_page_item').closest('.children').hide();
        $('.submenu-chapters').hide();
    }
});

// Disable glossary links
$('span.glossary-link a').click(function(e) {
    e.preventDefault();
});

// Greyscale
$(document).ready(function() {
    if (Cookies.get('greyscale') == 1) {
        $('html').addClass('greyscale');
    }
});

$('.js-greyscale').click(function(e) {

    e.preventDefault();

    if (Cookies.get('greyscale') == 1) {
        Cookies.set('greyscale', 0);
        $('html').removeClass('greyscale');
    }

    else {
        Cookies.set('greyscale', 1);
        $('html').addClass('greyscale');
    }
});

// Hide images
$(document).ready(function() {
    if (Cookies.get('hideimages') == 1) {
        $('img').hide();
    }
});

$('.js-hide-images').click(function(e) {

    e.preventDefault();

    if (Cookies.get('hideimages') == 1) {
        Cookies.set('hideimages', 0);
        $('img').show();
    }

    else {
        Cookies.set('hideimages', 1);
        $('img').hide();
    }
});

// Text size
$(document).ready(function() {
    $("html").css("font-size", Cookies.get('font-size'));
});

$('.js-increase-text').click(function(e) {

    e.preventDefault();

    var size = (parseInt($("html").css("font-size").replace(/px/,""))+3)+"px";

    Cookies.set('font-size', size);

    $("html").css("font-size", size);

});

$('.js-decrease-text').click(function(e) {

    e.preventDefault();

    var size = (parseInt($("html").css("font-size").replace(/px/,""))-3)+"px";

    Cookies.set('font-size', size);

    $("html").css("font-size", size);

});

// Search form
$('.js-search').click(function(e) {
	$('.search-form').slideToggle();
    e.preventDefault();
    $('.search-form input').focus();
    $('.search-form-bg').fadeToggle();
});
