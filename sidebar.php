<div class="hide-print">

    <?php $this_page_id = get_the_ID(); ?>

    <div class="mb-5">
        <a href="<?php echo site_url(); ?>">
            <img class="inline-block w-2/5 md:w-full" src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="logo">
        </a>

        <a class="md:hidden" href="<?php echo site_url(); ?>">
            <img class="ml-8 inline-block w-2/5" src="<?php echo get_template_directory_uri(); ?>/svg/mhcc-logo.svg" alt="mhcc logo">
        </a>
    </div>

    <ul class="submenu submenu-pages">
        <li>
            <a class="mb-2 text-black" href="#" style="background-color: #decee5;">
                Menu
            </a>
            <ul class="children mb-0">
                <?php $links = get_posts( 'title_li=&depth=1&post_type=page&post_parent=0&orderby=menu_order&order=ASC&posts_per_page=999&exclude=48' ); ?>
                <?php foreach ($links as $link) : ?>
                    <?php if (get_field('menu_title', $link->ID)) : ?>
                        <li class="<?php if ($this_page_id == $link->ID) { echo 'current_page_item'; } ?>"><a href="<?php echo get_permalink( $link->ID ); ?>"><?php the_field('menu_title', $link->ID); ?></a></li>
                    <?php else : ?>
                        <li class="<?php if ($this_page_id == $link->ID) { echo 'current_page_item'; } ?>"><a href="<?php echo get_permalink( $link->ID ); ?>"><?php echo $link->post_title; ?></a></li>
                    <?php endif; ?>
                <?php endforeach; ?>
            </ul>
        </li>
    </ul>

    <a class="bg-gray-400 block mb-2 leading-tight p-4 rounded text-sm text-gray-900 js-show-chapters md:hidden" href="#" style="background-color: #decee5;">Chapters</a>

    <ul class="submenu submenu-chapters mb-2 hidden md:block">
        <?php query_posts( 'post_type=chapter&post_parent=0&orderby=menu_order&order=ASC&posts_per_page=999' ); ?>
        <?php if ( have_posts() ) : ?>

            <?php while ( have_posts() ) : the_post(); ?>

                <li>
                    <a href="#">
                        <?php the_title(); ?>
                    </a>
                    <ul class="children mb-0">
                        <?php $links = get_posts( 'title_li=&depth=1&post_type=chapter&post_parent=0&orderby=menu_order&order=ASC&posts_per_page=999&post_parent=' . get_the_ID() ); ?>
                        <?php foreach ($links as $link) : ?>
                            <?php if (get_field('menu_title', $link->ID)) : ?>
                                <li class="<?php if ($this_page_id == $link->ID) { echo 'current_page_item'; } ?>"><a href="<?php echo get_permalink( $link->ID ); ?>"><?php the_field('menu_title', $link->ID); ?></a></li>
                            <?php else : ?>
                                <li class="<?php if ($this_page_id == $link->ID) { echo 'current_page_item'; } ?>"><a href="<?php echo get_permalink( $link->ID ); ?>"><?php echo $link->post_title; ?></a></li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                </li>

            <?php endwhile; ?>

        <?php endif; ?>
        <?php wp_reset_query(); ?>
    </ul>

    <ul class="submenu submenu-appendix mb-5">
        <li>
            <a class="mb-2 text-black" href="#" style="background-color: #decee5;">
                <?php echo get_the_title( 48 ); ?>
            </a>
            <ul class="children mb-0">
                <?php $links = get_posts( 'title_li=&depth=1&post_type=page&orderby=menu_order&order=ASC&posts_per_page=999&post_parent=48' ); ?>
                <?php foreach ($links as $link) : ?>
                    <?php if (get_field('menu_title', $link->ID)) : ?>
                        <li class="<?php if ($this_page_id == $link->ID) { echo 'current_page_item'; } ?>"><a href="<?php echo get_permalink( $link->ID ); ?>"><?php the_field('menu_title', $link->ID); ?></a></li>
                    <?php else : ?>
                        <li class="<?php if ($this_page_id == $link->ID) { echo 'current_page_item'; } ?>"><a href="<?php echo get_permalink( $link->ID ); ?>"><?php echo $link->post_title; ?></a></li>
                    <?php endif; ?>
                <?php endforeach; ?>
            </ul>
        </li>
    </ul>

    <a class="hide-small" href="<?php the_field('pdf', 4); ?>" target="_blank">
        <img class="mb-5" src="<?php the_field('image', 4); ?>" alt="poster">
    </a>

    <p class="hide-small text-center text-sm">
        <a href="<?php the_field('pdf', 4); ?>" target="_blank">
            Download your Mental Health Rights Manual poster here
        </a>
    </p>

</div>
