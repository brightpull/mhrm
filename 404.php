<?php get_header(); ?>

<div class="container py-12">

    <div class="grid-sidebar">

        <?php get_sidebar(); ?>

        <div class="content-area">

            <h2>THE REQUESTED PAGE COULD NOT BE FOUND</h2>

            <p>Please ensure you do not have any typos in the URL. Alternatively, click here to head to the homepage.</p>

        </div>

    </div>

</div>

<?php get_footer(); ?>
