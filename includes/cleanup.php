<?php

// Superfluous code
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'wp_generator' );

// Emoji scripts
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

// Image sizes
function mhrm_remove_image_sizes( $sizes )
{
    unset( $sizes['small'] );
    unset( $sizes['medium'] );
    unset( $sizes['large'] );
    return $sizes;
}
add_filter( 'intermediate_image_sizes_advanced', 'mhrm_remove_image_sizes' );

// WP version
function mhrm_remove_wp_version()
{
    return '';
}
add_filter( 'the_generator', 'mhrm_remove_wp_version' );
