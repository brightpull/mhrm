<?php

// Hide admin bar
show_admin_bar( false );

// Hide dashboard menu items
function mhrm_remove_admin_pages()
{
    remove_menu_page( 'edit.php' );
    // remove_menu_page( 'upload.php' );
    remove_menu_page( 'edit-comments.php' );
    // remove_menu_page( 'users.php' );
    // remove_menu_page( 'tools.php' );
    // remove_menu_page( 'plugins.php' );
    // remove_menu_page( 'options-general.php' );
    // remove_menu_page( 'themes.php' );
    // remove_menu_page( 'edit.php?post_type=acf-field-group' );
}
add_action( 'admin_menu', 'mhrm_remove_admin_pages' );
