<?php

function custom_post_types()
{
    $labels = array(
        'name'               => 'Chapters',
        'singular_name'      => 'Chapter',
        'menu_name'          => 'Chapters',
        'name_admin_bar'     => 'Chapter',
        'add_new'            => 'Add New',
        'add_new_item'       => 'Add New Chapter',
        'new_item'           => 'New Chapter',
        'edit_item'          => 'Edit Chapter',
        'view_item'          => 'View Chapter',
        'all_items'          => 'All Chapters',
        'search_items'       => 'Search Chapters',
        'parent_item_colon'  => 'Parent Chapter',
        'not_found'          => 'No Chapters Found',
        'not_found_in_trash' => 'No Chapters Found in Trash'
    );

    $args = array(
        'labels'              => $labels,
        'public'              => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'show_ui'             => true,
        'show_in_nav_menus'   => true,
        'show_in_menu'        => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-book',
        'capability_type'     => 'page',
        'hierarchical'        => true,
        'supports'            => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
        'has_archive'         => true,
        'rewrite'             => array( 'slug' => 'chapters' ),
        'query_var'           => true
    );

    register_post_type( 'chapter', $args );
}

add_action( 'init', 'custom_post_types' );
