<div class="u-pagination">

    <?php $all_chapters = array(); ?>

    <?php query_posts( 'posts_per_page=999&post_type=chapter&post_parent=0&orderby=menu_order&order=ASC' ); ?>

    <?php if ( have_posts() ) : ?>

        <?php while ( have_posts() ) : the_post(); ?>

            <?php $children = get_posts('posts_per_page=999&post_type=chapter&orderby=menu_order&order=ASC&post_parent=' . get_the_ID()); ?>

            <?php foreach($children as $child) : ?>

                <?php $all_chapters[] = $child->ID; ?>

            <?php endforeach; ?>

        <?php endwhile; ?>

    <?php endif; ?>

    <?php wp_reset_query(); ?>

    <?php $key = array_search($post->ID, $all_chapters); ?>

    <p>
        <?php if ( ! $key == 0) : ?>
        <a href="<?php echo get_permalink( $all_chapters[$key - 1] ); ?>">
            <?php echo get_template_part( 'svg/left' ); ?>
             &nbsp;
            Previous Page
        </a>
        <?php endif; ?>
    </p>

    <p>
        <?php if ( count($all_chapters) != $key + 1) : ?>
        <a href="<?php echo get_permalink( $all_chapters[$key + 1] ); ?>">
            Next Page
             &nbsp;
            <?php echo get_template_part( 'svg/right' ); ?>
        </a>
        <?php endif; ?>
    </p>

</div>
