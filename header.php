<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">

    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/print.css" type="text/css" media="print" />

    <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
    <![endif]-->

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="top"></div>

<a href="#top" class="hide-print back-to-top">
    <?php echo get_template_part( 'svg/up' ); ?>
</a>

<div class="search-form hidden">
    <div class="container flex justify-between items-center">
        <form action="<?php echo site_url(); ?>" method="get">
            <input type="search" name="s" placeholder="Search...">
        </form>
        <a class="js-search text-white" href="#">
            ✕
        </a>
    </div>
</div>

<div class="search-form-bg"></div>

<header class="hide-print bg-purple py-3 text-white">

    <div class="container flex justify-between">

        <ul class="mb-0">
            <li><a class="font-bold text-white" href="http://www.mhcc.org.au" target="_blank">MHCC Home</a></li>
            <li class="hide-small">|</li>
            <li class="hide-small"><a class="font-bold text-orange" href="<?php echo site_url(); ?>">Mental Health Rights Manual Home</a></li>
        </ul>

        <ul class="mb-0 accessibility"> <!-- Mobile -->
            <li>
                <a class="text-white js-search" href="#" target="_blank">
                    <?php echo get_template_part( 'svg/search' ); ?>
                    <span>Search</span>
                </a>
            </li>
        </ul>

        <ul class="mb-0 accessibility"> <!-- Desktop -->
            <li>
                <a class="text-white js-increase-text" href="#" target="_blank">
                    <?php echo get_template_part( 'svg/increase' ); ?>
                    <span>Increase</span>
                </a>
            </li>
            <li>
                <a class="text-white js-decrease-text" href="#" target="_blank">
                    <?php echo get_template_part( 'svg/decrease' ); ?>
                    <span>Decrease</span>
                </a>
            </li>
            <li>
                <a class="text-white js-greyscale" href="#" target="_blank">
                    <?php echo get_template_part( 'svg/greyscale' ); ?>
                    <span>Greyscale</span>
                </a>
            </li>
            <li>
                <a class="text-white js-hide-images" href="#" target="_blank">
                    <?php echo get_template_part( 'svg/images' ); ?>
                    <span>Remove Images</span>
                </a>
            </li>
            <li>
                <a class="text-white js-search" href="#" target="_blank">
                    <?php echo get_template_part( 'svg/search' ); ?>
                    <span>Search</span>
                </a>
            </li>
        </ul>

    </div>

</header>
