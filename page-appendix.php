<?php

/* Template Name: Appendix */

get_header(); ?>

<div class="container py-12">

    <div class="grid-sidebar">

        <?php get_sidebar(); ?>

        <div>

            <h2><?php the_title(); ?></h2>

            <?php query_posts( 'post_type=glossary&orderby=title&order=ASC&posts_per_page=999' ); ?>

            <?php if ( have_posts() ) : ?>

                <?php while ( have_posts() ) : the_post(); ?>

                    <h4 class="mb-3 text-orange"><?php the_title(); ?></h4>

                    <?php the_content(); ?>

                <?php endwhile; ?>

            <?php endif; ?>

            <?php wp_reset_query(); ?>

        </div>

    </div>

</div>

<?php get_footer(); ?>
