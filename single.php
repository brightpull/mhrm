<?php get_header(); ?>

<div class="container py-12">

    <div class="grid-sidebar">

        <?php get_sidebar(); ?>

        <div class="content-area">

            <?php if ( have_posts() ) : ?>

                <?php while ( have_posts() ) : the_post(); ?>

                    <p class="hide-small">
                        <a href="javascript:window.print()" class="text-purple flex items-center u-svg-12">
                            <?php echo get_template_part( 'svg/print' ); ?>
                            <span class="ml-2">Print this section</span>
                        </a>
                    </p>

                    <h2><?php the_title(); ?></h2>

                    <?php the_content(); ?>

                    <?php if ( get_field( 'updated' ) ) : ?>
                        <p class="mt-8 font-bold text-purple text-xs">Updated <?php the_field( 'updated' ); ?></p>
                    <?php endif; ?>

                    <?php echo get_template_part( 'parts/chapter-pagination' ); ?>

                <?php endwhile; ?>

            <?php endif; ?>

        </div>

    </div>

</div>

<?php get_footer(); ?>
