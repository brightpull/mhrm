<?php

get_template_part( 'includes/cleanup' );
get_template_part( 'includes/cpt' );
get_template_part( 'includes/customisations' );
get_template_part( 'includes/helpers' );

// Add theme support
add_theme_support( 'post-thumbnails' );
add_theme_support( 'title-tag' );

// Enqueue scripts
function mhrm_load_assets()
{
    // jQuery
    if ( ! is_admin() ) {
        wp_deregister_script( 'jquery' );
        wp_enqueue_script( 'jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js', array(), '3.4.1', true );
    }

    // CSS
    wp_enqueue_style( 'app', get_template_directory_uri() . '/style.css', null, '1.0.5' );

    // JavaScript
    wp_enqueue_script( 'js-cookie', 'https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.2.0/js.cookie.min.js', array(), '2.2.0', true );
    wp_enqueue_script( 'app', get_template_directory_uri() . '/js/app-min.js', array(), null, true );
}
add_action( 'wp_enqueue_scripts', 'mhrm_load_assets' );

// Sibling links
function siblings( $link ) {

    global $post;

    $siblings = get_pages( 'child_of=' . $post->post_parent . '&post_type=chapter&parent=' . $post->post_parent );

    foreach ( $siblings as $key=>$sibling ) {
        if ($post->ID == $sibling->ID) {
            $ID = $key;
        }
    }

    $closest = array( 'before' => get_permalink( $siblings[$ID-1]->ID ), 'after' => get_permalink( $siblings[$ID+1]->ID ) );

    if ( $link == 'before' || $link == 'after' ) {
        echo $closest[$link];
    } else { return $closest; }
}
