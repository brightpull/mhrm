<?php get_header(); ?>

<div class="container py-12">

    <div class="grid-sidebar">

        <?php get_sidebar(); ?>

        <div>

            <h2>Search results</h2>

            <?php if ( have_posts() ) : ?>

                <?php while ( have_posts() ) : the_post(); ?>

                    <div class="mb-5" style="border-bottom: 1px solid #d7d7d7;">

                        <h4 class="mb-3 text-orange"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h4>

                        <?php the_excerpt(); ?>

                    </div>

                <?php endwhile; ?>

            <?php endif; ?>

        </div>

    </div>

</div>

<?php get_footer(); ?>
