<?php get_header(); ?>

<div class="container py-12">

    <div class="grid-sidebar">

        <?php get_sidebar(); ?>

        <div class="content-area">

            <div class="hidden md:flex items-end mb-10">

                <h3 class="mb-1">Mental Health Coordinating Council</h3>

                <img class="ml-12" width="200" src="<?php echo get_template_directory_uri(); ?>/svg/mhcc-logo.svg" alt="mhcc logo">

            </div>

            <?php if ( have_posts() ) : ?>

                <?php while ( have_posts() ) : the_post(); ?>

                    <p class="hide-small">
                        <a href="javascript:window.print()" class="text-purple flex items-center u-svg-12">
                            <?php echo get_template_part( 'svg/print' ); ?>
                            <span class="ml-2">Print this section</span>
                        </a>
                    </p>

                    <?php the_content(); ?>

                    <?php if ( get_field( 'updated' ) ) : ?>
                        <p class="mt-8 font-bold text-purple text-xs">Updated <?php the_field( 'updated' ); ?></p>
                    <?php endif; ?>

                <?php endwhile; ?>

            <?php endif; ?>

        </div>

    </div>

</div>

<?php get_footer(); ?>
